# ATLAS

A static website built showcasing **ATLAS** (Mayones Regius 7) with a Guitar Fretboard and Metronome. 😃

**Atlas**'s website is live on: [atlas-guitar.vercel.guitarFretboard](https://atlas-guitar.vercel.app). 😃