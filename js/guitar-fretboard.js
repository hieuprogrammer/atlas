(() => {
    const root = document.documentElement;
    const fretboard = document.querySelector(`.fretboard`);
    const instrumentSelector = document.querySelector('#instrument-selector');
    const accidentalSelector = document.querySelector('.accidental-selector');
    const numberOfFretsSelector = document.querySelector('#number-of-frets-selector');
    const showAllNotesSelector = document.querySelector(`#show-all-notes-selector`);
    const showMultipleNotesSelector = document.querySelector(`#show-multiple-notes-selector`);
    const notes = document.querySelector(`.notes`);
    const singleDotFretmarksPositions = [3, 5, 7, 9, 15, 17, 19, 21];
    const doubleDotsFretmarksPositions = [12, 24];
    const flatNotes  = [`C`, `Db`, `D`, `Eb`, `E`, `F`, `Gb`, `G`, `Ab`, `A`, `Bb`, `B`];
    const sharpNotes = [`C`, `C#`, `D`, `D#`, `E`, `F`, `F#`, `G`, `G#`, `A`, `A#`, `B`];
    let allNotesWithSamePitch;
    let showAllNotesWithSamePitch = false;
    let showAllNotes = false;
    const tunings = {
        'Guitar (6 strings)': [4, 11, 7, 2, 9, 4],
        'Guitar (7 strings)': [9, 4, 11, 7, 2, 9, 4],
        'Bass (4 strings)': [7, 2, 9, 4],
        'Bass (5 strings)': [7, 2, 9, 4, 11],
        'Ukulele': [9, 4, 0, 7],
    };
    let selectedInstrument = 'Guitar (6 strings)';
    let numberOfFrets = 24;
    let numberOfStrings = tunings[selectedInstrument].length;
    let accidentals = `sharps`;

    const tools = {
        createElement(element, content) {
            element = document.createElement(element);
            if (arguments.length > 1)
                element.innerHTML = content;

            return element;
        }
    };

    const guitarFretboard = {
        generateNotes(noteIndex, accidentals) {
            noteIndex = noteIndex % 12;
            let notes;
            if (accidentals === `flats`) {
                notes = flatNotes[noteIndex];
            } else if (accidentals === `sharps`) {
                notes = sharpNotes[noteIndex];
            }

            return notes;
        },

        setupFretboard() {
            fretboard.innerHTML = ``;
            root.style.setProperty(`--number-of-strings`, numberOfStrings);

            // Add frets to fretboard:
            for (let index = 0; index < numberOfStrings; index++) {
                let string = tools.createElement(`div`);
                string.classList.add(`string`);
                fretboard.appendChild(string);

                // Create frets:
                for (let fret = 0; fret <= numberOfFrets; fret++) {
                    let note = tools.createElement(`div`);
                    note.classList.add(`note`);
                    note.classList.add(`fw-bold`);
                    string.appendChild(note);

                    let noteName = this.generateNotes((fret + tunings[selectedInstrument][index]), accidentals);
                    note.setAttribute(`data-note`, noteName);

                    // Add single dot inlays:
                    if (index === 0 && singleDotFretmarksPositions.indexOf(fret) !== -1) {
                        note.classList.add(`single-dot-fretmark`);
                    }

                    if (index === 0 && doubleDotsFretmarksPositions.indexOf(fret) !== -1) {
                        let doubleDotsFretMark = tools.createElement(`div`);
                        doubleDotsFretMark.classList.add(`double-dots-fretmark`);
                        note.appendChild(doubleDotsFretMark);
                    }
                }
            }

            allNotesWithSamePitch = document.querySelectorAll(`.note`);
        },

        setupInstrumentSelector() {
            for (let tuning in tunings) {
                let instrumentOption = tools.createElement(`option`, tuning);
                instrumentOption.classList.add(`fw-bold`);
                instrumentSelector.appendChild(instrumentOption);
            }
        },

        setupNoteNames() {
            notes.innerHTML = ``;
            let noteNames;
            if (accidentals === `sharps`) {
                noteNames = sharpNotes;
            } else {
                noteNames = flatNotes;
            }

            noteNames.forEach((note) => {
                let noteHtmlElement = tools.createElement(`span`, note);
                notes.appendChild(noteHtmlElement);
            });
        },



        toggleShowAllNotesWithSamePitch(note, opacity) {
            for (let index = 0; index < allNotesWithSamePitch.length; index++) {
                if (allNotesWithSamePitch[index].dataset.note === note) {
                    allNotesWithSamePitch[index].style.setProperty(`--note-dot-opacity`, opacity);
                }
            }
        },

        init() {
            this.setupFretboard();
            this.setupInstrumentSelector();
            this.setupNoteNames();
            eventHandlers.setupEventListeners();
        }
    };

    const eventHandlers = {
        showNotes(event) {
            // Check if show all notes is selected:
            if (showAllNotes)
                return;

            if (event.target.classList.contains(`note`)) {
                if (showMultipleNotesSelector) {
                    guitarFretboard.toggleShowAllNotesWithSamePitch(event.target.dataset.note, 1);
                } else {
                    event.target.style.setProperty(`--note-dot-opacity`, 1);
                }
            }
        },

        hideNotes(event) {
            // Check if show all notes is selected:
            if (showAllNotes)
                return;

            if (showMultipleNotesSelector) {
                guitarFretboard.toggleShowAllNotesWithSamePitch(event.target.dataset.note, 0);
            } else {
                event.target.style.setProperty(`--note-dot-opacity`, 0);
            }
        },

        setSelectedInstrument(event) {
            selectedInstrument = event.target.value;
            numberOfStrings = tunings[selectedInstrument].length;
            guitarFretboard.setupFretboard();
        },

        setAccidentals(event) {
            if (event.target.classList.contains(`selected-accidental`)) {
                accidentals = event.target.value;
                guitarFretboard.setupFretboard();
                guitarFretboard.setupNoteNames();
            } else {
                return;
            }
        },

        setNumberOfFrets() {
            numberOfFrets = numberOfFretsSelector.value;
            guitarFretboard.setupFretboard();
        },

        toggleShowAllNotes() {
            showAllNotes = showAllNotesSelector.checked;
            if (showAllNotes) {
                root.style.setProperty(`--note-dot-opacity`, 1);
                guitarFretboard.setupFretboard();
            } else {
                root.style.setProperty(`--note-dot-opacity`, 0);
                guitarFretboard.setupFretboard();
            }
        },

        toggleShowMultipleNotes() {
            showAllNotesWithSamePitch = !showAllNotesWithSamePitch;
        },

        showNotesOnMouseOver(event) {
            let noteToShow = event.target.innerText;
            guitarFretboard.toggleShowAllNotesWithSamePitch(noteToShow, 1);
        },

        hideNotesOnMouseOver(event) {
            if (!showAllNotes) {
                let noteToHide = event.target.innerText;
                guitarFretboard.toggleShowAllNotesWithSamePitch(noteToHide, 0);
            } else {
                return;
            }
        },

        setupEventListeners() {
            fretboard.addEventListener(`mouseover`, this.showNotes);
            fretboard.addEventListener(`mouseout`, this.hideNotes);
            instrumentSelector.addEventListener(`change`, this.setSelectedInstrument);
            accidentalSelector.addEventListener(`click`, this.setAccidentals);
            numberOfFretsSelector.addEventListener(`change`, this.setNumberOfFrets);
            showAllNotesSelector.addEventListener(`change`, this.toggleShowAllNotes);
            showMultipleNotesSelector.addEventListener(`change`, this.toggleShowMultipleNotes);
            notes.addEventListener(`mouseover`, this.showNotesOnMouseOver);
            notes.addEventListener(`mouseout`, this.hideNotesOnMouseOver);
        },
    }

    guitarFretboard.init();
})();