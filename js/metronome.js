import Timer from './timer.js';

const tempoDisplay = document.querySelector(`.tempo`);
const tempoText = document.querySelector(`.tempo-text`);
const decreaseTempoBtn = document.querySelector(`.decrease-tempo`);
const increaseTempoBtn = document.querySelector(`.increase-tempo`);
const tempoSlider = document.querySelector(`.slider`);
const startStopBtn = document.querySelector(`.start-stop`);
const subtract1BpmBtn = document.querySelector(`.subtract-1-bpm`);
const add1BpmBtn = document.querySelector(`.add-1-bpm`);
const bpmCount = document.querySelector(`.bpm-count`);

const click1 = new Audio(`./click1.mp3`);
const click2 = new Audio(`./click2.mp3`);

let bpm = 250;
let beatsPerMinute = 1;
let count = 0;
let isRunning = false;
let tempoTextString = `Normal`;

decreaseTempoBtn.addEventListener(`click`, () => {
    if (bpm <= 1) return;

    bpm--;
    validateTempo();
    updateMetronome();
});

increaseTempoBtn.addEventListener(`click`, () => {
    if (bpm >= 500) return;

    bpm++;
    validateTempo();
    updateMetronome();
});

tempoSlider.addEventListener(`input`, () => {
    bpm = tempoSlider.value;
    validateTempo();
    updateMetronome();
});

subtract1BpmBtn.addEventListener(`click`, () => {
    if (beatsPerMinute <= 1) return;

    beatsPerMinute--;
    bpmCount.textContent = beatsPerMinute;
    count = 0;
});

add1BpmBtn.addEventListener(`click`, () => {
    if (beatsPerMinute >= 100) return;

    beatsPerMinute++;
    bpmCount.textContent = beatsPerMinute;
    count = 0;
});

startStopBtn.addEventListener(`click`, () => {
    count = 0;

    if (!isRunning) {
        metronome.start();
        isRunning = true;
        startStopBtn.textContent = `STOP`;
    } else {
        metronome.stop();
        isRunning =  false;
        startStopBtn.textContent = `START`;
    }
});

function updateMetronome() {
    tempoDisplay.textContent = bpm;
    tempoSlider.value = bpm;
    metronome.timeInterval = 60000 / bpm;

    if (bpm <= 40) { tempoTextString = `Let's Start Walking`; }
    else if (bpm > 40 && bpm < 80) { tempoTextString = `Slow`; }
    else if (bpm >= 80 && bpm < 120) { tempoTextString = `Getting there`; }
    else if (bpm >= 120 && bpm < 180) { tempoTextString = `Normal`; }
    else if (bpm >= 180 && bpm < 220) { tempoTextString = `Nice and Steady`; }
    else if (bpm >= 220 && bpm < 240) { tempoTextString = `Rock n' Roll`; }
    else if (bpm >= 240 && bpm < 260) { tempoTextString = `Funky`; }
    else if (bpm >= 260 && bpm < 280) { tempoTextString = `Relax Dude`; }
    else if (bpm >= 280 && bpm < 300) { tempoTextString = `Eddie Van Halen`; }
    else if (bpm >= 300) { tempoTextString = `Blazing`; }

    tempoText.textContent = tempoTextString;
}

function validateTempo() {
    if (bpm <= 1) return;
    if (bpm >= 500) return;
}

function playClick() {
    if (count === beatsPerMinute) count = 0;

    if (count === 0) {
        click1.play();
        click1.currentTime = 0;
    } else {
        click2.play();
        click2.currentTime = 0;
    }

    count++;
}

const metronome = new Timer(playClick, 60000 / bpm, { immediate: true });